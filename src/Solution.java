import java.util.*;

/**
 * Solution class.
 *
 * @author Festim Shyti
 */
public class Solution {

    private class PositionComparator implements Comparator<Node> {
        public int compare(Node node0, Node node1) {
            return (node0.position < node1.position)
                    ? -1
                    : (node0.position == node1.position)
                    ? (node0.weight == node1.weight)
                    ? Integer.compare(node0.durability, node1.durability) * -1
                    : Integer.compare(node0.weight, node1.weight)
                    : 1;
        }
    }

    private class Tree {
        public ArrayList<Node> nodes;

        public Tree() {
            nodes = new ArrayList<Node>();
        }

        public boolean addNode(Node node) {
            if (node.position == -1) return nodes.add(node);
            if (nodes.isEmpty()) return false;

            for (Node n : nodes) {
                if (addNode(n, node)) return true;
            }

            return false;
        }

        private boolean addNode(Node parent, Node node) {
            if (parent.durability < parent.totalWeight + node.weight) return false;

            if (parent.left == null) parent.left = node;
            else if (parent.right == null) parent.right = node;
            else {
                if (!addNode(parent.left, node))
                    if (!addNode(parent.right, node)) return false;
            }

            parent.totalWeight += node.weight;
            return true;
        }
    }

    private class Node {

        public int durability;
        public int weight;
        public int position;

        public int totalWeight;

        public Node left;
        public Node right;

        public Node(int durability, int weight, int position) {
            this.durability = durability;
            this.weight = weight;
            this.position = position;
            this.totalWeight = weight;
        }

        public String toString() {
            return new StringBuilder().append(durability).append(",").append(weight).append(",").append(position).toString();
        }
    }

    private ArrayList<Node> getSortNodes(ArrayList<Node> nodes) {
        return getSortNodes(nodes, new ArrayList<Node>(), null);
    }

    private ArrayList<Node> getSortNodes(ArrayList<Node> nodes, ArrayList<Node> partNodes, Node ln) {
        if (!nodes.isEmpty() && nodes.get(0).position != -1) return partNodes;

        for (Node node : nodes) {
            if (ln == null) {
                partNodes.add(node);
                ln = node;
                continue;
            }
            if (ln.position == node.position) continue;
            if (ln.position + 1 == node.position) {
                partNodes.add(node);
                ln = node;
            } else break;
        }

        nodes.removeAll(partNodes);
        if (!nodes.isEmpty()) {
            if (nodes.get(0).position == ln.position) return partNodes;
            else return getSortNodes(nodes, partNodes, null);
        }
        return partNodes;
    }

    /**
     * Solution.
     * Solution of problem.
     *
     * @param A Durability array.
     * @param B Weight array.
     * @param C Position array.
     * @return integer Result.
     */
    public int solution(int[] A, int[] B, int[] C) {
        ArrayList<Node> nodes = new ArrayList<Node>();
        for (int i = 0; i < C.length; i++) nodes.add(new Node(A[i], B[i], C[i]));
        Collections.sort(nodes, new PositionComparator());
        ArrayList<Node> sortNodes = getSortNodes(nodes);

        Tree tree = new Tree();

        int c = 0;
        for (Node node : sortNodes) {
            if (!tree.addNode(node)) break;
            c++;
        }

        return c;
    }

    public int getNodeDepth(Node node, int d) {
        if (node == null) return d;
        return Math.max(getNodeDepth(node.left, d+1), getNodeDepth(node.right, d+1));
    }

    public void printNode(ArrayList<Node> nodes) {
        for (Node node : nodes) System.out.println(node);
    }

    public void printNode(Node node, int depth) {
        if (node == null) return;

        String depthStr = "";
        for (int d = 0; d < getNodeDepth(node, 0); d++) depthStr += "  ";

        System.out.println(depthStr + "   |");
        System.out.println(depthStr + node);
        if (node.left != null) printNode(node.left, depth-1);
        if (node.right != null) printNode(node.right, depth+1);
    }

    public void printTree(Tree tree) {
        System.out.println("Print tree node:");
        for (Node node : tree.nodes)
            printNode(node, getNodeDepth(node,0));
    }

    /**
     * Main.
     * Startup method for test solutions.
     *
     * @param args
     */
    public static void main(String[] args) {
        Solution solution = new Solution();
        int result;

        int[] A = {5, 3, 6, 3, 3};
        int[] B = {2, 3, 1, 1, 2};
        int[] C = {-1, 0, -1, 0, 3};

        result = solution.solution(A, B, C);
        System.out.println("Solution result for example 1 is: " + result);

        int[] A2 = {4, 3, 1};
        int[] B2 = {2, 2, 1};
        int[] C2 = {-1, 0, 1};

        result = solution.solution(A2, B2, C2);
        System.out.println("Solution result for example 2 is: " + result);

        int[] A3 = {8, 10, 5, 8, 7, 5, 6, 6, 10, 7};
        int[] B3 = {1, 4, 1, 3, 3, 4, 1, 1, 5, 5};
        int[] C3 = {-1, 0, -1, 2, 0, 0, 0, 6, 0, 1};

        result = solution.solution(A3, B3, C3);
        System.out.println("Solution result for example 3 is: " + result);


        int[] A4 = {7, 9, 5, 8, 8, 6, 8, 5, 10, 6};
        int[] B4 = {3, 3, 2, 4, 1, 3, 3, 4, 1, 5};
        int[] C4 = {-1, -1, 0, -1, 3, 1, 2, 4, -1, -1};

        result = solution.solution(A4, B4, C4);
        System.out.println("Solution result for example 4 is: " + result);

        int[] A5 = {9, 6, 6, 10, 7, 7, 7, 7, 6, 8};
        int[] B5 = {2, 4, 3, 3, 2, 5, 2, 3, 3, 5};
        int[] C5 = {-1, 0, -1, 1, 0, 2, 0, -1, 4, 8};

        result = solution.solution(A5, B5, C5);
        System.out.println("Solution result for example 5 is: " + result);
    }
}